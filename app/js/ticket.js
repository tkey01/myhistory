function moreInfo(){
	if($(".more-info-icon").length !== 0){
		var coords = $(".more-info-icon")[0].getBoundingClientRect();

		$(".more-info").css({
			'top': coords.top + -110 + 'px',
			'left': coords.left + -185 + 'px'
		});
	}
}

$(document).ready(function(){
	//Иконка больше информации в слайдере
	moreInfo();

	//Календарь на <= 640px
	$("#calendar-1").datepicker({
		inline: true,
		showOtherMonths: true,
      selectOtherMonths: true
	});

	$("#calendar-2").datepicker({
		inline: true,
		showOtherMonths: true,
      selectOtherMonths: true
	});

	//Слайдеры билеты
   $(".change-date").slick({
   	arrows: false,
   	infinite: false,
   	slidesToShow: 3,
   	variableWidth: false,
   	responsive: [
   	 {
   	 	breakpoint: 901,
   	 	settings: {
   	 		slidesToShow: 2,
   	 		variableWidth: false
   	 	}
   	 }
   	]
   });

   $(".change-product").slick({
   	arrows: false,
   	infinite: false,
   	slidesToShow: 4,
   	responsive: [
   	 {
   	 	breakpoint: 901,
   	 	settings: {
   	 		slidesToShow: 3,
   	 	}
   	 }
   	]
   });

   $(".change-project").slick({
   	arrows: false,
   	infinite: false,
   	slidesToShow: 2
   });

   $(".change-abon").slick({
   	arrows: false,
   	infinite: false,
   	slidesToShow: 2
   });

 	//Пагинаторы слайдеров
 //  	$('.change-date').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	// 	if (nextSlide == 0){
	// 		$('.ticket-slider-control .ticket-slider-1-prev').addClass('disabled');
	// 	}
	// 	else{
	// 		$('.ticket-slider-control .ticket-slider-1-prev').removeClass('disabled');
	// 	}

	// 	if ( ((nextSlide == 1) && (window.matchMedia("screen and (min-width: 901px)").matches))
	// 	||	((nextSlide == 2) && (window.matchMedia("screen and (max-width: 900px)").matches)) ){
	// 		$('.ticket-slider-control .ticket-slider-1-next').addClass('disabled');
	// 	}
	// 	else{
	// 		$('.ticket-slider-control .ticket-slider-1-next').removeClass('disabled');
	// 	}
	// });

	$('.ticket-slider-1-prev').on('click',function(){ $('.change-date').slick('slickPrev'); });
	$('.ticket-slider-1-next').on('click',function(){ $('.change-date').slick('slickNext'); });

	$('.ticket-slider-2-prev').on('click',function(){ $('.change-product').slick('slickPrev'); });
	$('.ticket-slider-2-next').on('click',function(){ $('.change-product').slick('slickNext'); });

	$('.ticket-slider-3-prev').on('click',function(){ $('.change-project').slick('slickPrev'); });
	$('.ticket-slider-3-next').on('click',function(){ $('.change-project').slick('slickNext'); });

	$('.ticket-slider-4-prev').on('click',function(){ $('.change-abon').slick('slickPrev'); });
	$('.ticket-slider-4-next').on('click',function(){ $('.change-abon').slick('slickNext'); });

	$('.change-date').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var count_slides = $('.change-date .item').length;

		if(nextSlide == 0){
			$('.ticket-slider-1-prev').addClass('disabled');
		}
		else{
			$('.ticket-slider-1-prev').removeClass('disabled');
		}

		if ( ((nextSlide == count_slides - 3) && (window.matchMedia("screen and (min-width: 901px)").matches))
		||	((nextSlide == count_slides - 2) && (window.matchMedia("screen and (max-width: 900px)").matches)) ){
			$('.ticket-slider-1-next').addClass('disabled');
		}
		else{
			$('.ticket-slider-1-next').removeClass('disabled');
		}
	});

	$('.change-product').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var count_slides = $('.change-product .item').length;

		if(nextSlide == 0){
			$('.ticket-slider-2-prev').addClass('disabled');
		}
		else{
			$('.ticket-slider-2-prev').removeClass('disabled');
		}

		if ( ((nextSlide == count_slides - 4) && (window.matchMedia("screen and (min-width: 901px)").matches))
		||	((nextSlide == count_slides - 3) && (window.matchMedia("screen and (max-width: 900px)").matches)) ){
			$('.ticket-slider-2-next').addClass('disabled');
		}
		else{
			$('.ticket-slider-2-next').removeClass('disabled');
		}
	});

	$('.change-project').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var count_slides = $('.change-project .item').length;

		if(nextSlide == 0){
			$('.ticket-slider-3-prev').addClass('disabled');
		}
		else{
			$('.ticket-slider-3-prev').removeClass('disabled');
		}

		if ( ((nextSlide == count_slides - 2) && (window.matchMedia("screen and (min-width: 901px)").matches))
		||	((nextSlide == count_slides - 2) && (window.matchMedia("screen and (max-width: 900px)").matches)) ){
			$('.ticket-slider-3-next').addClass('disabled');
		}
		else{
			$('.ticket-slider-3-next').removeClass('disabled');
		}
	});

	$('.change-abon').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var count_slides = $('.change-abon .item').length;

		if(nextSlide == 0){
			$('.ticket-slider-4-prev').addClass('disabled');
		}
		else{
			$('.ticket-slider-4-prev').removeClass('disabled');
		}

		if ( ((nextSlide == count_slides - 2) && (window.matchMedia("screen and (min-width: 901px)").matches))
		||	((nextSlide == count_slides - 2) && (window.matchMedia("screen and (max-width: 900px)").matches)) ){
			$('.ticket-slider-4-next').addClass('disabled');
		}
		else{
			$('.ticket-slider-4-next').removeClass('disabled');
		}
	});

	//Промокод всплывающее окно
	$(".item-3-promo>.img>img").hover(
		function(){
			$(".promo-hover").css({'opacity':'1'});
		},
		function(){
			$(".promo-hover").css({'opacity':'0'});
		}
	);

	$(".change-product .more-info-icon").hover(
		function(){
			$(".more-info").css({'opacity':'1'});
		},
		function(){
			$(".more-info").css({'opacity':'0'});
		}
	);
});

$(window).scroll(function(){
	moreInfo();
});