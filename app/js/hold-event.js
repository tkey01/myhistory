$(document).ready(function(){
	$("#datepicker-1").datepicker({
		showOtherMonths: true,
      selectOtherMonths: true,
      dateFormat: "d M yy",
      setDate: 'today'
	});

	$("#datepicker-2").datepicker({
		showOtherMonths: true,
      selectOtherMonths: true,
      dateFormat: "d M yy",
      setDate: 'today'
	});

	$("#hour-1").selectmenu().selectmenu("menuWidget").addClass('forselect');
	$("#hour-2").selectmenu().selectmenu("menuWidget").addClass('forselect');

});