var popup = '.popup';

//// скрипт закрывающий форму
$('[data-btn-type="close"]').on('click', function (e) {
    e.preventDefault();
    $('body').removeClass('overflow-h');
    $(popup).addClass('popup_hidden');
    $(popup).find('iframe').remove();
    $(popup + '>div:not(.popup__layout)').addClass('hidden');
});

//// скрипт открывающий форму
$('[data-popup]').on('click', function (e) {
    e.preventDefault();
    $('body').addClass('overflow-h');
    $(popup).removeClass('popup_hidden');
    $($(this).data('popup')).removeClass('hidden');
});

$('[data-popup-video]').on('click', function (e) {
    e.preventDefault();
    $('body').addClass('overflow-h');
    $(popup).removeClass('popup_hidden');
    $($(this).data('popup-video')).removeClass('hidden');

    var videoId = $(this).attr('href');
    var videoTpl = '<iframe src="https://www.youtube.com/embed/'+ videoId +'?ecver=2&autoplay=1&controls=1" width="100%" height="100%" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe>';
    $($(this).data('popup-video')).find('.popup__video-container').append(videoTpl);
});


//Карта
var map = {
    'msk': {
        coords: [13.5, 36],
        city: 'Москва'
    },
    'ekat1': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat2': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat3': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat4': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat5': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat6': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat7': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ekat8': {
        coords: [27.5, 53.5],
        city: 'Екатеринбург'
    },
    'ufa': {
        coords: [23, 59],
        city: 'Уфа'
    }
};

for (pin in map){
    $('.map-select-640').append('<option value="'+ pin +'">'+ map[pin].city +'</option>');
    $('.map-pin').append('<img src="img/icons/marker-big.png" class="map-marker active '+ pin +'" data-map-pin="'+ pin +'">');
    $('.map-select-item').append('<li><a class="map-select map-'+ pin +'" data-map-select-pin="'+ pin +'">'+ map[pin].city +'</a></li>');
    $('.city-slider').append('<a class="map-select map-'+ pin +'" data-map-select-pin="'+ pin +'">'+ map[pin].city +'</a>');
    $('.map-pin .map-marker.' + pin).css({"left": + map[pin].coords[0] + "%", "top": + map[pin].coords[1] + "%"});
}

$('[data-map-select-pin]').on('click', function (e) {
    var data = $(this).data('map-select-pin');
    var pin = $('[data-map-pin="'+ data +'"]');
    $('[data-map-select-pin]').removeClass('active');
    $(this).addClass('active');
    $('[data-map-pin]').removeClass('active');
    pin.attr('src', 'img/icons/marker-big1.png');
    pin.addClass('active');
    pin.css({"left": + map[data].coords[0] + "%", "top": + map[data].coords[1] + "%"});
});

$('.map-select-640').on('change', function() {
    var data = this.value;
    var pin = $('[data-map-pin="'+ data +'"]');
    $('[data-map-pin]').removeClass('active');
    pin.attr('src', 'img/icons/marker-big1.png');
    pin.addClass('active');
    pin.css({"left": + map[data].coords[0] + "%", "top": + map[data].coords[1] + "%"});
});

//Карта google
function initMap() {
    var position = {lat: 55.834393, lng: 37.626210};
    var center = {lat: position.lat -0.001, lng: position.lng};
    var map = new google.maps.Map(document.getElementById('map'), {
        center: position,
        scrollwheel: false,
        zoom: 17,
        //styles: style,
        //disableDefaultUI: true,
    });
    var beachMarker = new google.maps.Marker({
        position: position,
        map: map,
        icon: 'img/loc.png'
    });
}

if($('#map').length !== 0){
    ymaps.ready(init);
}

var myMap;

function init(){     
    myMap = new ymaps.Map("map", {
        center: [55.834393, 37.626210],
        zoom: 16,
        controls: []
    },
    {
        suppressMapOpenBlock: true
    }
    );

    myPlacemark = new ymaps.Placemark([55.834393, 37.626210], { 
        hintContent: 'Исторический парк «Россия — моя история»', 
        balloonContent: 'Исторический парк на ВДНХ' 
    },
    {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: 'img/loc.png',
        // Размеры метки.
        iconImageSize: [35, 50],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [2, -40]
    });

    myMap.geoObjects.add(myPlacemark);
    myMap.controls.add('zoomControl');
}