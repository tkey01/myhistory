//Адаптив блоков
function resizeBlock(){
	if(window.matchMedia("screen and (max-width: 640px)").matches){
	  $(".menu-900").insertBefore($(".header"));

	  $('.slider-2 .text').each(function(i,elem){
	  	$(this).find('hr').prependTo(this);
	  	$(this).find('.bottom-text').prependTo(this);
	  });

	  $('.slider-2 .fact-slide').each(function(i,elem){
	  	$(this).find('.fact-text').appendTo(this);
	  });
	}
	else if (window.matchMedia("screen and (min-width: 641px)").matches){
	  $(".menu-900").insertAfter($(".header"));

	  $('.slider-2 .text').each(function(i,elem){
	  	$(this).find('hr').appendTo(this);
	  	$(this).find('.bottom-text').appendTo(this);
	  });

	  $('.slider-2 .fact-slide').each(function(i,elem){
	  	$(this).find('.fact-text-1').prependTo('.text-1');
	  	$(this).find('.fact-text-2').prependTo('.text-2');
	  	$(this).find('.fact-text-3').prependTo('.text-3');
	  	$(this).find('.fact-text-4').prependTo('.text-4');
	  });
	}

	var scrolled = window.pageYOffset || document.documentElement.scrollTop;

	if(window.matchMedia("screen and (max-width: 1220px)").matches){
		$('.menu-logo-wr').css({
			'display': 'none'
		});
		$('.menu').css({
			'width': '75%',
			'padding-left':'40px'
		});
	}
	else if ((window.matchMedia("screen and (min-width: 1221px)").matches) && (scrolled >= 700)){
		$('.menu-logo-wr').css({
			'display': 'block'
		});
		$('.menu').css({
			'width': '62.5%',
			'padding-left':'20px'
		});
	}
}


//Функции для пагинаторов слайдеров
var countSlide_1 = $('.event-slide').length;
var countSlide_2 = $('.feedback .item').length;

function reControl_1(){
	if(window.matchMedia("screen and (min-width: 641px)").matches) {
	  if(countSlide_1 <= 3){
	    $('.slider-1-control').css({"display": "none"});
	  }
	}
	else{
  	 $('.slider-1-control').css({"display": "block"});
  }
}

function reControl_2(){
	if(window.matchMedia("screen and (min-width: 900px)").matches) {
	  if(countSlide_2 <= 3){
	    $('.slider-3-control').css({"display": "none"});
	  }
	}
	else{
  	 $('.slider-3-control').css({"display": "block"});
  }
}


//Раскрывающееся меню
$(".menu-toggle").click(function(){
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$(".menu-900 .lang").css({'display':'block'});
		$(".menu-900 .logo-2").css({'display':'none'});
		$(".menu-900-ul .soc-menu").css({'margin-bottom':'0'});
		$(".menu-900-ul .lang-640").css({'display':'none'});
	}
	else{
		$(this).addClass('active');
		$(".menu-900 .lang").css({'display':'none'});
		$(".menu-900 .logo-2").css({'display':'block'});
		$(".menu-900-ul .soc-menu").css({'margin-bottom':'20px'});
		$(".menu-900-ul .lang-640").css({'display':'block'});
	}

   $('.menu-900-ul').slideToggle(400);
});

$(".menu-2-toggle").click(function(){
	if($(this).hasClass('active')){
		$(this).removeClass('active');
	}
	else{
		$(this).addClass('active');
	}

   $('.menu-2-900-ul').slideToggle(400);
});

//Выбор города всплывающее окно
$(".close-city").click(function(){
	$(".city").css({
		'display' : 'none'
	});
});


//При загрузке страницы
$(document).ready(function(){
	$('.menu-full').Stickyfill();
	$('.menu-960').Stickyfill();
	$('.menu-2-full').Stickyfill();
	$('.menu-2-960').Stickyfill();

	resizeBlock();

	reControl_1();
	reControl_2();

	$(".slider-1").slick({
		slidesToShow: 3,
		arrows: false,
		infinite: true,
		responsive: [
	    {
	      breakpoint: 641,
	      settings: {
	        slidesToShow: 1
	      }
	    }
	  ]
   });

   $(".slider-2").slick({
		slidesToShow: 1,
		vertical: true,
		autoplay: false,
		speed: 1000,
		autoplaySpeed: 100,
		arrows: false,
		infinite: false,
		verticalSwiping: true,
		adaptiveHeight: true,
		responsive: [
	    {
	      breakpoint: 1221,
	      settings: {
	        verticalSwiping: false,
	        vertical: false
	      }
	    }
	  ]
   });

   $(".feedback").slick({
		slidesToShow: 3,
		arrows: false,
		infinite: false,
		responsive: [
	    {
	      breakpoint: 901,
	      settings: {
	        slidesToShow: 2
	      }
	    },
	    {
	      breakpoint: 641,
	      settings: {
	        slidesToShow: 1,
	        adaptiveHeight: true
	      }
	    }
	  ]
   });

   $(".partner-slider").slick({
   	arrows: false,
   	infinite: false,
   	responsive: [
   	 {
   	 	breakpoint: 901,
   	 	settings: {
   	 		slidesToShow: 7,
   	 		touchThreshold: 15
   	 	}
   	 }
   	]
   });

   $(".exhibition-section-6 .project-items").slick({
   	//arrows: false,
   	infinite: false,
   	slidesToShow: 3,
   	responsive: [
   	 {
   	 	breakpoint: 640,
   	 	settings: {
   	 		slidesToShow: 1,
   	 	}
   	 }
   	]
   });

   $(".city-slider").slick({
   	arrows: false,
   	infinite: false,
   	slidesToShow: 2
   });

   $(".header-640-slider").slick({
   	arrows: false,
   	dots: true,
   	infinite: true,
   	slidesToShow: 1
   });


   //Пагинаторы слайдеров главная
   $('.header-slider-paginator').on('click',function(){ $('.header-video').slick('slickNext'); });

   $('.slider-1-prev').on('click',function(){ $('.slider-1').slick('slickPrev'); });
	$('.slider-1-next').on('click',function(){ $('.slider-1').slick('slickNext'); });

	$('.slider-3-prev').on('click',function(){ $('.feedback').slick('slickPrev'); });
	$('.slider-3-next').on('click',function(){ $('.feedback').slick('slickNext'); });

	$('.slider-2-1').on('click',function(){ 
		$('.slider-2').slick('slickGoTo',0);
	});
	$('.slider-2-2').on('click',function(){ 
		$('.slider-2').slick('slickGoTo',1);
	});
	$('.slider-2-3').on('click',function(){ 
		$('.slider-2').slick('slickGoTo',2); 
	});
	$('.slider-2-4').on('click',function(){ 
		$('.slider-2').slick('slickGoTo',3); 
	});

	$('.slider-2').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var active = nextSlide + 1;
		$('.slider-2-control a').removeClass('active');
		$('.slider-2-'+active).addClass('active');
	});

	$('.slider-4-1').on('click',function(){ 
		$('.partner-slider').slick('slickGoTo',0); 
	});

	$('.slider-4-2').on('click',function(){ 
		$('.partner-slider').slick('slickGoTo',1); 
	});

	$('.slider-4-3').on('click',function(){ 
		$('.partner-slider').slick('slickGoTo',2); 
	});

	$('.slider-4-4').on('click',function(){ 
		$('.partner-slider').slick('slickGoTo',3); 
	});

	$('.slider-4-5').on('click',function(){ 
		$('.partner-slider').slick('slickGoTo',4); 
	});

	$('.partner-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var active = nextSlide + 1;
		$('.partner-slider-paginator a').removeClass('active');
		$('.slider-4-'+active).addClass('active');
	});

	$('.city-slider-control>.left-arrow').on('click',function(){
		$('.city-slider').slick('slickPrev');
	});

	$('.city-slider-control>.right-arrow').on('click',function(){
		$('.city-slider').slick('slickNext');
	});

	$('.city-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var count_slides = $('.city-slider .map-select').length;

		$('.city-slider-control>.left-arrow').css('display', 'block');
		$('.city-slider-control>.right-arrow').css('display', 'block');

		if(nextSlide == count_slides - 2){
			$('.city-slider-control>.right-arrow').css('display', 'none');
		}

		if(nextSlide == 0){
			$('.city-slider-control>.left-arrow').css('display', 'none');
		}
	});

	$('.slider-about-prev').on('click',function(){ $('.feedback').slick('slickPrev'); });
	$('.slider-about-next').on('click',function(){ $('.feedback').slick('slickNext'); });

	$('.feedback').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var count_slides = $('.feedback .item').length;

		if(nextSlide == 0){
			$('.slider-3-prev').addClass('disabled');
		}
		else{
			$('.slider-3-prev').removeClass('disabled');
		}

		if ( ((nextSlide == count_slides - 3) && (window.matchMedia("screen and (min-width: 901px)").matches))
		||	((nextSlide == count_slides - 2) && (window.matchMedia("screen and (max-width: 900px)").matches) && (window.matchMedia("screen and (min-width: 641px)").matches))
		|| ((nextSlide == count_slides - 1) && (window.matchMedia("screen and (max-width: 640px)").matches)) ){
			$('.slider-3-next').addClass('disabled');
		}
		else{
			$('.slider-3-next').removeClass('disabled');
		}
	});


	//Поиск pop-up
	//Главная
	$('.menu-right>.search>input').focus(function(){
		$('.search-popup, .search').addClass('focus');
	});

	$('.menu-right>.search>input').focusout(function(){
		$('.search-popup, .search').removeClass('focus');
	});

	$('.menu-search').click(function(){
		if(!$('.menu-search').hasClass('focus')){
			$('.search-popup-900, .menu-search').addClass('focus');
		}
		else{
			$('.search-popup-900, .menu-search').removeClass('focus');
		}
	});

	//Внутренние
	$('.menu-2-right>.search>input').focus(function(){
		$('.search-popup, .search').addClass('focus');
	});

	$('.menu-2-right>.search>input').focusout(function(){
		$('.search-popup, .search').removeClass('focus');
	});

	$('.menu-2-search').click(function(){
		if(!$('.menu-2-search').hasClass('focus')){
			$('html, body').css("overflow","hidden");
			$('.search-popup-900, .menu-2-search').addClass('focus');
		}
		else{
			$('html, body').css("overflow","auto");
			$('.search-popup-900, .menu-2-search').removeClass('focus');
		}
	});


	//Select
	if($('#blog-select-1').length !== 0){
		$('#blog-select-1').selectmenu().selectmenu("menuWidget").addClass('forselect2');
	}

	if($('#blog-select-2').length !== 0){
		$('#blog-select-2').selectmenu().selectmenu("menuWidget").addClass('forselect2');
	}

	if($('#news-select-1').length !== 0){
		$('#news-select-1').selectmenu().selectmenu("menuWidget").addClass('forselect2');
	}

	if($('#news-select-2').length !== 0){
		$('#news-select-2').selectmenu().selectmenu("menuWidget").addClass('forselect2');
	}

	if($('#poster-select-1').length !== 0){
		$('#poster-select-1').selectmenu().selectmenu("menuWidget").addClass('forselect2');
	}

	if($('#poster-select-2').length !== 0){
		$('#poster-select-2').selectmenu().selectmenu("menuWidget").addClass('forselect2');
	}
	
});


//При ресайзе страницы
$(window).resize(function(){
   resizeBlock();

   reControl_1();
	reControl_2();
});


//При скроле страницы
$(window).scroll(function(){
	var scrolled = window.pageYOffset || document.documentElement.scrollTop;

	if((scrolled >= 700) && (window.matchMedia("screen and (min-width: 1221px)").matches)){
		$('.menu-logo-wr').css({
			'display': 'block'
		});
		$('.menu').css({
			'width': '62.5%',
			'padding-left': '20px'
		});
	}
	else{
		$('.menu-logo-wr').css({
			'display': 'none'
		});
		$('.menu').css({
			'width': '75%',
			'padding-left': '40px'
		});
	}

	if((scrolled >= 700) && (window.matchMedia("screen and (min-width: 901px)").matches)){
		$('.menu-full').css('box-shadow', '0 40px 120px rgba(0, 0, 0, 0.5)');
	}
	else{
		$('.menu-full').css('box-shadow', 'none');
	}

	if( ((scrolled >= 960) && (window.matchMedia("screen and (min-width: 641px)").matches))
	|| ((scrolled >= 340) && (window.matchMedia("screen and (max-width: 640px)").matches)) ) {
		$(".menu-900").addClass("menu-900-scrolled");
	}
	else{
		$(".menu-900").removeClass("menu-900-scrolled");
	}

	if(scrolled >= 1){
		$('.menu-2-full').css('box-shadow', '0 40px 120px rgba(0, 0, 0, 0.5)');
		$('.menu-2-900').css('box-shadow', '0 20px 80px rgba(0, 0, 0, 0.4)');
	}
	else{
		$('.menu-2-full').css('box-shadow', 'none');
		$('.menu-2-900').css('box-shadow', 'none');
	}
});

var changeSlide = function (q,w,e) {
	w.$slider.find('.slide-video').html('');
	if($(w.$slides[e]).find('.slide-video').length !== 0){
		var videoId = w.$slider.find('.slide-video').data('video');
		var videoTpl = '<div class="video-bg-wrap"><video autoplay loop muted class="video-bg" id="video"><source src="'+ videoId +'.mp4" type="video/mp4"><source src="'+ videoId +'.webm" type="video/webm"></video></div>';

		$(w.$slides[e]).find('.slide-video').html(videoTpl);
		w.$slider.find('video')[0].play();
	}
};


$('.index-header-slider').slick({
	arrows: false,
	autoplay: true,
	autoplaySpeed: 4500,
	speed: 1,
	dots: true,
	pauseOnHover: false,
	pauseOnDotsHover: false,
	pauseOnFocus: false,
	swipe: true,
	fade: true,
  	cssEase: 'linear'
});


if($('.header-video').length !== 0){
	$('.header-video').on('afterChange', changeSlide);
	$('.header-video').find('video')[0].play();
}


$('.header-video').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	var number_index_slide = nextSlide;
	$('.index-header-slider').slick('slickGoTo', number_index_slide);
});